import Main.Cafe
import org.scalatest.FlatSpec

class StandardBillTest extends FlatSpec {

  "A List as input" should "return as output final price" in {
    val res1: Double = Main.standardBill(List(Cafe("cola", 0.5), Cafe("coffee", 1), Cafe("cheese", 2)))
    assert (res1 == 3.5)
    val res2: Double = Main.standardBill(List(Cafe("cola", 0.5), Cafe("coffee", 1), Cafe("cheese", 2), Cafe("stake", 4.5)))
    assert (res2 == 8)
    val res3: Double = Main.standardBill(List())
    assert (res3 == 0)
  }

  "A List as input" should "return as output final price with service charges" in {
    val res4: Double = Main.serviceCharge(List(Cafe("cola", 0.5), Cafe("coffee", 1)))
    assert (res4 == 1.5)
    val res5: Double = Main.serviceCharge(List(Cafe("cola", 0.5), Cafe("coffee", 1), Cafe("cheese", 2)))
    assert (res5 == 3.85)
    val res6: Double = Main.serviceCharge(List(Cafe("cola", 0.5), Cafe("coffee", 1), Cafe("cheese", 2), Cafe("stake", 4.5)))
    assert (res6 == 9.6)
    val res7: Double = Main.serviceCharge(List(Cafe("cola", 0.5), Cafe("coffee", 1), Cafe("coffee", 1), Cafe("coffee", 1), Cafe("coffee", 1), Cafe("coffee", 1), Cafe("cheese", 2), Cafe("cheese", 2), Cafe("cheese", 2),
      Cafe("cheese", 2), Cafe("cheese", 2), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5),
      Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5), Cafe("stake", 4.5)))
    assert (res7 == 125.5)
    val res8: Double = Main.serviceCharge(List())
    assert (res8 == 0)
  }
}

