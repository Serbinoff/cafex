object Main extends App {
  case class Cafe(name: String, price: Double)

  def standardBill(list: List[Cafe]) = list.map(x => x.price).sum

  def serviceCharge(list: List[Cafe]) = {
    val sum = list.map(x => x.price).sum
    if (!list.contains(Cafe("cheese", 2)) && !list.contains(Cafe("stake", 4.5))) sum
    else if (!list.contains(Cafe("stake", 4.5))) sum + sum * 0.1
    else sum + (if (sum * 0.2 < 20) sum * 0.2 else 20)
  }
}
